# Task_12_Create_basic_app



Create the application with basic functionality using result of previous task.

The plan:
Set up environment, tools(virtualenv, IDE, e.t.c.)
Set up deploying tools for heroku 
Create models: User, Photo etc.
Add views and routes 
Add functional of storing photos and generate previews. 
Fill a fake data into the database, generate migrations, etc.
Deploy the code to the server.
Write tests using Unittest module or py.test.


Resources:
Django https://docs.djangoproject.com/en/3.0/
https://django-debug-toolbar.readthedocs.io/en/latest/
https://django-extensions.readthedocs.io/en/stable/index.html
https://sunscrapers.com/blog/10-django-packages-you-should-know/

Deploy code using the following cloud providers

Every cloud provider has free services or a free period of use. You can cancel it at any time.

AWS Free Tier
https://aws.amazon.com/free/?all-free-tier.sort-by=item.additionalFields.SortRank&all-free-tier.sort-order=asc&awsf.Free%20Tier%20Types=*all&awsf.Free%20Tier%20Categories=*all
Google Cloud Free Program
https://cloud.google.com/free/docs/free-cloud-features
Azure
https://azure.microsoft.com/en-us/pricing/free-services/